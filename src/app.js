var express = require("express");
require("express-async-errors");

const http = require("http");
const config = require("./config");
const pino = require("pino");
const expressPino = require("express-pino-logger");
const bodyParser = require("body-parser");

var app = express();
const server = http.createServer(app);
const logger = pino({ level: config.LOG_LEVEL });

app
  .use(expressPino({ logger }))
  .use(express.json({ limit: "50mb" }))
  .use(bodyParser.json())
  .use("/status", require("./probe"))
  .use("/api/users", (req, res) => {
    return res.sendStatus(400);
  })
  .use("/api/users/:_id", (req, res) => {
    return res.sendStatus(200);
  });

if (process.env.NODE_ENV !== "test") {
  listen().catch((err) =>
    logger.warn("failed to listen to kafka, error:", err)
  );
  server.listen(config.PORT, config.HOST, () => {
    logger.info(`Listening to ${config.HOST}:${config.PORT}`);
  });
}

module.exports = {
  app,
  server,
};
