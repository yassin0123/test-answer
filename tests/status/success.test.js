const _ = require("lodash");
const Promise = require("bluebird");
const request = require("supertest");
const { app, server } = require("../../src/app");

const headers = {
  Accept: "application/json",
};

const withToken = (token) => ({
  ...headers,
  token,
});

describe("SUCCESS : test", () => {
  afterEach(() => {
    return server.close();
  });

  it("SUCCESS : status ", async () => {
    await request(app).get("/status").set(headers).expect(200);
  });

  it("SUCCESS : users ", async () => {
    await request(app).get("/api/users").set(headers).expect(400);
  });

  it("SUCCESS : one user ", async () => {
    await request(app).get("/api/users/:some_id").set(headers).expect(200);
  });
});
